package pageObjectModelFaceBook;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class POMfacebook {

	WebDriver driver;
	
	//Facebook Elements
	public By Email = By.id("email");  

	private String FBEmailAddress="wallethub4@gmail.com";

	By Password = By.id("pass");

	private String FBUserPassword="WalletHubAutomation";

	By FBLogin = By.id("loginbutton");  

	public By Profile = By.cssSelector("#u_0_a > div:nth-child(1) > div:nth-child(1) > div");  

	public By TextArea = By.cssSelector("#js_uw > div._i-o._2j7c > div > div.clearfix._ikh > div._4bl9 > div > div");  

	public By InputArea = By.cssSelector("#js_2 > div._i-o._2j7c > div > div.clearfix._ikh > div._q-v._4bl7 > div");  

    public By ComposePost = By.linkText("Compose Post");

	public String statusMSG= "Hello World";

	By Post = By.xpath("//*[@data-testid='react-composer-post-button']");





	public POMfacebook(WebDriver driver) {
		this.driver = driver;
	}

	//*******Login To Facebook
	public void setUserName(String strUserName){

		driver.findElement(Email).sendKeys(FBEmailAddress);

	}


	//Set password in password textbox

	public void setPassword(String strPassword){

		driver.findElement(Password).sendKeys(FBUserPassword);

	}

	//Click on login button

	public void clickLogin(){

		driver.findElement(FBLogin).click();

	}


	public void ChooseComposePost(){

		driver.findElement(ComposePost).click();

	}
	
	public void PostStatusMSG(){

		driver.findElement(Post).click();

	}

	/**

	 * This POM method will be exposed in test case to login in Facebook

	 * @param strUserName

	 * @param strPasword

	 * @return

	 */

	public void LoginFB(String strUserName,String strPasword){

		//Fill Email

		this.setUserName(strUserName);

		//Fill password

		this.setPassword(strPasword);

		//Click Login button

		this.clickLogin();        

	}

}
