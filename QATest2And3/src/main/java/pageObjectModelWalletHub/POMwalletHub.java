package pageObjectModelWalletHub;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class POMwalletHub {

	WebDriver driver;

	//WalletHub Login Elements
	public By WalletHubEmail = By.cssSelector("#join-login > form > div:nth-child(5) > input");  

	private String WalletHubEmailAddress="wallethub2@gmail.com";

	By WalletHubPassword = By.cssSelector("#join-login > form > div:nth-child(6) > input");

	private String WalletHubUserPassword="WalletHubAutomation$1";

	By WalletHubLogin = By.cssSelector("#join-login > form > div.btns > button.btn.blue.touch-element-cl");  

	public By StarsRating = By.xpath("//*[@id='wh-body-inner']/div[2]/div[3]/span");
			
	public By ForthStar = By.xpath("//*[@id='wh-body-inner']/div[2]/div[3]/div[1]/div/a[4]");
			
	public By FifthStar = By.xpath("//*[@id='wh-body-inner']/div[2]/div[3]/div[1]/div/a[5]");
	
	By PolicyDropDown = By.cssSelector("#reviewform > div.product > div");
			
	By PolicyValue = By.linkText("Health");
	
	public By PolicyRate = By.id("overallRating");

	public By FifthStarPolicyRate = By.xpath("//*[@id='overallRating']/a[5]");

	public By ReviewInputArea = By.id("review-content");
	
	public String ReviewFixedPart= "Test data , review in unique comment very execution 'enter some data dfdfdfdfdfdfdfdfdfdfdfdfdfdfdfdf dffdfdfdfdfdfdfdfdfdfdfdfdfdfsdsdsd sdsdsdsdsd aqwedszxcvfrtg'";
	
	public By SubmitReview = By.cssSelector("#reviewform > div.content > div.submit > input");
	
	public By PostedReview = By.cssSelector("#reviewform > div.content.small > p");

	public By UserMenu = By.cssSelector("#viewport > header > div > nav.right-menu.loggedIn > a.user");

	public By UserProfile = By.xpath("//*[@id='m-user']/ul/li[4]/a");

	
			
	public POMwalletHub(WebDriver driver) {
		this.driver = driver;
	}

	//*******Login To WalletHub

	public void setUserName2(String strUserName2){

		driver.findElement(WalletHubEmail).sendKeys(WalletHubEmailAddress);

	}
	//Set password in password textbox

	public void setPassword2(String strPassword2){

		driver.findElement(WalletHubPassword).sendKeys(WalletHubUserPassword);

	}


	//Click on login button

	public void clickLogin2(){

		driver.findElement(WalletHubLogin).click();

	}

	public void OpenPolicyDropDown(){

		driver.findElement(PolicyDropDown).click();

	}

	public void ChoosePolicyValue(){

		driver.findElement(PolicyValue).click();

	}
	
	
	public void ClearReviewInputArea(){

		driver.findElement(ReviewInputArea).clear();

	}
	
	public void SubmitReview(){

		driver.findElement(SubmitReview).click();

	}
	
	
	/**

	 * This POM method will be exposed in test case to login in WalletHub

	 * @param strUserName

	 * @param strPasword

	 * @return

	 */

	public void LoginWalletHub(String strUserName2,String strPasword2){

		//Fill Email

		this.setUserName2(strUserName2);

		//Fill password

		this.setPassword2(strPasword2);

		//Click Login button

		this.clickLogin2();        

	}
}
