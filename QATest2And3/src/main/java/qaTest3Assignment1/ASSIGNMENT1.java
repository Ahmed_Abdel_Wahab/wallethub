package qaTest3Assignment1;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import pageObjectModelFaceBook.POMfacebook;

/*
 * ***********************HINT************************************************
 * Kindly make sure that facebook doesn't suspicious account activity
 * If so change Login Email address and password to new one at POMfacebook.java
 * 
*/
public class ASSIGNMENT1 {


	WebDriver driver;
	//Object from page object model'POM' that hold used elements as test case scenario element's factory
	POMfacebook objPOM; 
	@org.testng.annotations.BeforeTest


	public void setup() throws InterruptedException{
		//ChromeDriver included into Resource folder
		System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		//Prevent notifications
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		//Driver is to pop up
		options.setHeadless(false);
		driver = new ChromeDriver(options);
		//implicit wait
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.get("http://www.facebook.com");
		Thread.sleep(400);

	}

	@org.testng.annotations.Test

	public void FacebookAssignmnet1() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver,25000);
		objPOM = new POMfacebook(driver);
		//Wait till element appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.Email));
		//Execute login in page object model
		//Email address and password can be changed from their variables through POM
		objPOM.LoginFB("", "");
		//Wait till profile appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.Profile));
		//Access profile
		driver.findElement(objPOM.Profile).click();
		//Wait till compose post appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.ComposePost));
		//Open Post area
		objPOM.ChooseComposePost();
		//The problem in post elements in facebook are: all class, div and IDs are changing every session
		//BUT elements IDs to access consist of :'fixed&unique string + generated string'
		//Which is ' placeholder + 'some generated string ' so here trying to access any id with that part of fixed&unique string 
		//I didn't include that cssSelector value in page object model just to be viewable when explaining how it works
		WebElement InputAreaElement=driver.findElement(By.cssSelector("*[id^='placeholder-']"));
		//Access and push data using Action library to avoid issues in 'focus element'
		Actions actions = new Actions(driver);
		actions.moveToElement(InputAreaElement);
		actions.click();
		/*********************Hint***************************
		 * ******Facebook sometimes show validation message if posted message identical,
		 * with same content already posted before,
		 * to avoid it , gonna add some unique generated string to the message posted. 
		 * If you want to remove it , just set send keys to 'Hello world ' only
		 * Generate random number of 5 digits and add it to the message
		*/
		long number = (long) Math.floor(Math.random() * 90000L) + 10000L;
		String StatusMessage="Hello World "+number;
		actions.sendKeys(StatusMessage);
		actions.build().perform();
		//Wait till fully loaded
		Thread.sleep(5000);
		//Post status
		objPOM.PostStatusMSG();
		//Wait to see the result
		Thread.sleep(9000);

	}

	//Automated taking screenshot once failure as an  if something expectedly happens
	//Save screenshots using method name and any other declarative methods in pic name and save in 'error screenshots' folder
	@org.testng.annotations.AfterMethod 
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException, InterruptedException { 
		if (testResult.getStatus() == ITestResult.FAILURE) { 
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
					+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		}

	}
	//close driver
	@org.testng.annotations.AfterTest

	public void End(){

		driver.close();
	}




}