package qaTest3Assignment2;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.Assert;

import pageObjectModelWalletHub.POMwalletHub;

public class ASSIGNMENT2 {

	WebDriver driver;
	
	//Object from page object model'POM' that hold used elements as test case scenario element's factory
	POMwalletHub objPOM; 
	
	
	@org.testng.annotations.BeforeTest
	
	public void setup() throws InterruptedException{
		//ChromeDriver included into Project Resource folder 
		System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
		//Prevent chrome notifications  
		Map<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", prefs);
		//Driver set to pop up
		options.setHeadless(false);
		driver = new ChromeDriver(options);
		//implicit wait
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		//Navigate to login page
		driver.get("https://wallethub.com/join/login");
		Thread.sleep(400);

	}

	@org.testng.annotations.Test

	public void WalletHubAssignment2() throws InterruptedException
	{
		WebDriverWait wait = new WebDriverWait(driver,25000);
		objPOM = new POMwalletHub(driver);
		//After Navigating to login page , perform login process with defined credentials in POM
		objPOM.LoginWalletHub("", "");
		//Wait till login process is complete
		Thread.sleep(4000);
		driver.get("http://wallethub.com/profile/test_insurance_company/");
		//Wait till element appearance
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.StarsRating));
		
		/*********************Hint***************************
		 *Make sure that 'Get your free credit and score' pop-up isn't viewed as it may lead test case to fail,
		 *As it makes stars rate hidden and not visible to web driver
		*/
		
		//Hover over rating slide
		Actions action=new Actions(driver);
		WebElement stars=driver.findElement(objPOM.StarsRating);
		action.moveToElement(stars).build().perform();
		//Hover over Forth Star
		stars=driver.findElement(objPOM.ForthStar);
		action.moveToElement(stars).build().perform();
		//Wait to see hover 
		Thread.sleep(1000);
		//Hover to fifth star and choose it
		stars = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(objPOM.FifthStar));
		stars.click();
		//Click on policy Drop-down list
		objPOM.OpenPolicyDropDown();
		Thread.sleep(500);
		//Choose policy
		objPOM.ChoosePolicyValue();
		Thread.sleep(1000);
		//Choose number of stars for that policy
		stars=driver.findElement(objPOM.PolicyRate);
		action.moveToElement(stars).build().perform();
		//Hover to fifth star and choose it for that policy
		stars = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(objPOM.FifthStarPolicyRate));
		stars.click();
		Thread.sleep(500);
		//Clear Review area if it has content for previous review.
		objPOM.ClearReviewInputArea();
		Thread.sleep(500);
		//UUID library generate 36 character unique set every execution
		//Doing so in order to make sure entered review message is unique every execution
		String EnteredReview=UUID.randomUUID().toString()+objPOM.ReviewFixedPart;
		//Enter Review
        driver.findElement(objPOM.ReviewInputArea).sendKeys(EnteredReview);
		Thread.sleep(1000);
		//Submit
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.SubmitReview));
		objPOM.SubmitReview();
		wait.until(ExpectedConditions.visibilityOfElementLocated(objPOM.PostedReview));
		//Make sure review has been submitted
		String Review = driver.findElement(objPOM.PostedReview).getText();
		//Hover to user
		stars=driver.findElement(objPOM.UserMenu);
		action.moveToElement(stars).build().perform();
		//choose user profile
		stars = (new WebDriverWait(driver, 5)).until(ExpectedConditions.presenceOfElementLocated(objPOM.UserProfile));
		stars.click();
		Thread.sleep(2000);
		//Check that review same as the one been entered and added successfully and assert true if so.
		//Hard assertion If False then throw exception
		Assert.assertTrue(Review.contains(EnteredReview));
        System.out.println("Test scenario executed and review submitted successfully!");

	
	}		


	//Automated taking screenshot once failure as an  if something expectedly happens
	//Save screenshots using method name and any other declarative methods in pic name and save in 'error screenshots' folder
	@org.testng.annotations.AfterMethod 
	
	public void takeScreenShotOnFailure(ITestResult testResult) throws IOException, InterruptedException { 
		if (testResult.getStatus() == ITestResult.FAILURE) { 
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
			FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
					+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
		}

	}
	
	//close driver
	@org.testng.annotations.AfterTest

	public void End(){

		driver.close();
	}





}
